var el =
    '<div class="form-group cab-config-block">' +
    '<label><input id="cab-config-label--%theId%" class="form-control" name="cab-config-label--%theId%" required></label><br/>' +
    '<label><strong>CSS</strong></label>' +
    '<input id="rapProdCssLink--%theId%" class="form-control" name="rapProdCssLink--%theId%"  required>' +
    '<label><strong>Extension</strong></label>' +
    '<input id="rapProdExtensionLink--%theId%" class="form-control" name="rapProdExtensionLink--%theId%"  required>' +
    '<label><strong>Blocks</strong></label>' +
    '<input id="rapProdBlocksLink--%theId%" class="form-control" name="rapProdBlocksLink--%theId%"  required>' +
    '</div>';

$(function () {
    // retrieve values that will be sent to the server
    window.getValues = function () {
        var values = {},
            inputs = document.querySelectorAll('input');

        inputs.forEach(function (input) {
            var name = input.name;

            if (name) {
                var inputValue = input.value;
                values[name] = inputValue;
            }
        });

        return values;
    };
}(jQuery));

$(function () {

    // populate inputs when configuration is loaded
    window.setValues = function (values) {
        var amountOfBlocks = [];
        Object.keys(values).forEach(function (key, index) {
            // key: the name of the object key
            // index: the ordinal position of the key within the object

            var identifierPos = key.indexOf("--");
            if (identifierPos !== -1) {
                var identifier = key.slice(identifierPos + 2);
                if (amountOfBlocks.indexOf(identifier) == -1) {
                    amountOfBlocks.push(identifier);
                }
            }

        });
        if (amountOfBlocks.length > 0) {
            amountOfBlocks.sort(function(a, b){return a-b});
            for (var i = 0; i < amountOfBlocks.length; i++) {
                var element = el;
                element = element.replace(/%theId%/g, i);
                $('.panel-body').append($(element));
            }
        }
        var inputs = document.querySelectorAll('input');

        inputs.forEach(function (input) {
            var name = input.name;

            if (name) {
                var value = values && values[name],
                    $input = $(input);

                $input.val(value);
                $input.trigger('change');
            }
        });
    };
}(jQuery));

$(function () {
    //Add button
    $('#addComponentButton').on('click', function () {
        var previousBlocks = $(".cab-config-block").length;

        var element = el;
        element = element.replace(/%theId%/g, previousBlocks);

        $('.panel-body').append($(element));
        $('.cab-config-block input').each(function (index, value) {
            var id = $(this).attr("id");
            $('body').trigger('setup-component', id);

        });
    });
});