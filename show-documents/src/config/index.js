// config/index.js
(function () {
    const router = require('router');
    const appData = require('appData');
    const logUtil = require('LogUtil');
    const globalAppData = require('globalAppData');

    router.get('/', (req, res) => {
        var profiles = [];
        var profile = true;
        var x = 0;

        // Get all available profiles
        while (profile && x < 100) {
            profile = globalAppData.get("cab-config-label--" + x);
            if (profile == null)
                profile = false;
            else
                profiles.push(profile);

        x++;
        }

        res.render({profiles: profiles});
    });
})();