(function () {
  'use strict';

  const router = require('router');
  const appData = require('appData');
  const globalAppData = require('globalAppData');
  const logUtil = require('LogUtil');
  const portletContextUtil = require('PortletContextUtil');
  const nodeIteratorUtil = require('NodeIteratorUtil');
  const properties = require('Properties');

  router.get('/', (req, res) => {
    var profileDefinition = appData.get("profile"),
      searchOrDetail = appData.get("searchOrDetail"),
      county = appData.get("county");

    var currentPage = portletContextUtil.getCurrentPage();
    var children = nodeIteratorUtil.getMenuItems(currentPage);
    var child,
      childUri = "";

    if (children.hasNext()) {
      child = children.next();
      childUri = properties.get(child, "URI");
    }

    var cssLink = null,
      extensionLink = null,
      blocksLink = null;

    var profiles = [];
    var profile = true;
    var x = 0;

    // Get all available profiles
    while (profile && x < 100) {
      

      profile = globalAppData.get("cab-config-label--" + x);
      if (profile == null)
        profile = false;
      else
        profiles.push(profile);

        x++;
    }
    // Check if the input profile is the same as one defined
    var choosenProfileIndex = profiles.indexOf(profileDefinition);
    if(choosenProfileIndex !== -1){
      cssLink = globalAppData.get("rapProdCssLink--" + choosenProfileIndex),
      extensionLink = globalAppData.get("rapProdExtensionLink--" + choosenProfileIndex),
      blocksLink = globalAppData.get("rapProdBlocksLink--" + choosenProfileIndex);
    }
    var resultObject = {
      searchOrDetail: searchOrDetail,
      cssLink: cssLink,
      extensionLink: extensionLink,
      blocksLink: blocksLink,
      childUri: childUri,
      county: county
    };
    res.render('/', resultObject);
  });
})();